
MAX_PLAYERS = 10
noms = []
puntuacio = []

for player in range(MAX_PLAYERS):
    nom = input("Nom? ")
    noms.append(nom)

playeractual = 0
for x in range(MAX_PLAYERS):
    puntuacio.append(input(f"Puntuació de {noms[playeractual]}: "))
    playeractual += 1

print("\nJUGADORS: ")
for jugador in sorted(noms):
    print(jugador)

print("\nGUANYADOR: ")
for punts, player in sorted(zip(puntuacio, noms), reverse=True):
    print(punts, player)


