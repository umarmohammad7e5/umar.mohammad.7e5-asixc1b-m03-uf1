"""
UMAR MOHAMMAD RIAZ
20/10/2021
ASIXc 1B

Crea un programa que llegeixi un nombre per pantalla i mostri en pantalla un missatge que digui si és positiu,
negatiu o igual a zero.

"""

numero = float(input("Introdueix un numero: "))

if numero == 0:
    print("Igual a ZERO")
elif numero > 0:
    print("POSITIU")
else:
    print("NEGATIU")

print("\nPrograma finalitzat")
