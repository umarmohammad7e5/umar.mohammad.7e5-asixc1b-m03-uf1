from time import sleep

SLEEP = 5

for var in range(1, 11):
    sleep(SLEEP)
    print(var, " ", end="")
