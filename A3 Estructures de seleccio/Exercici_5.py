"""
UMAR MOHAMMAD RIAZ
20/10/2021
ASIXc 1B

Realitza un programa que demani un nom d'usuari i una contrasenya. Si l'usuari és "pepe" i la contrasenya "asdasd",
el programa mostrarà el missatge "Has entrat a el sistema". En cas contrari es mostrarà un missatge d'error
(com per exemple, "Usuari / password incorrecte")

"""

USUARI = "pepe"
KEY = "asdasd"

usuari_entrat = input("USUARI: ")
key_entrat = input("CONTRASEÑA: ")

if USUARI != usuari_entrat:
    print("\nUsuari incorrecte")
if KEY != key_entrat:
    print("Password incorrecte")
elif USUARI == usuari_entrat and KEY == key_entrat:
    print("\nHas entrat al sistema!")

print("\nPROGRAMA FINALITZAT")
