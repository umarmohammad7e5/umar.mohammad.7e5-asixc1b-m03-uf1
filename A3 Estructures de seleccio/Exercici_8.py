"""
UMAR MOHAMMAD RIAZ
20/10/2021
ASIXc 1B

Escriu un programa que demani a l'usuari per teclat dos nombres, 'nota' i 'edat', i un caràcter per indicar el gènere.
Al programa caldrà implementar la següent lògica:
Si la nota és igual o major que 5 i l'edat és més gran o igual a 18 i el gènere és 'F', llavors es mostrarà el missatge
‘Acceptada’
Si en canvi es compleix el mateix, però amb gènere igual a 'M', llavors es mostrarà el missatge 'Possible'.
En qualsevol altre cas (és a dir, no es compleixen ni les condicions del primer punt ni les del segon) s'ha de
mostrar el missatge 'No Acceptada'.

"""


nota = float(input("La teva nota: "))
edat = float(input("La teva edat: "))
genere = input("Introduiex el teu genere [M o F]: ")

if nota >= 5 and edat >= 18 and genere.upper() == "F":
    print("\n ACCEPTADA")
elif nota >= 5 and edat >= 18 and genere.upper() == "M":
    print("\n POSSIBLE")
else:
    print("\n NO ACCEPTADA")

print("\n \n PROGRAMA FINALITZAT")

