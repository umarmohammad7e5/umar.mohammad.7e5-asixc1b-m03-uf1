"""
UMAR MOHAMMAD RIAZ
02/11/2021
ASIXc 1B

Escriu un programa que donat un dia de la setmana (com a número, de l'1 a l'7), escrigui
el dia corresponent en lletres. Si introduïm un nombre que no sigui de l'1 a l'7, el programa mostrarà un
missatge d'error.

Extra bonus: Modificar el programa per a tres idiomes: Anglès, Castellà i Català
"""

idioma_usuari = ""

while True:
    idioma_usuari = input("\nENGLISH, CATALA O CASTELLANO (Q to quit): ")
    if idioma_usuari.lower() == "q":
        exit()
    elif idioma_usuari.lower() == "english":
        print(idioma_usuari.upper())
        break
    elif idioma_usuari.lower() == "catala":
        print(idioma_usuari.upper())
        break
    elif idioma_usuari.lower() == "castellano":
        print(idioma_usuari.upper())
        break
    else:
        print(f"\n{idioma_usuari} LANGUAGE INCORRECT, IDIOMA INCORRECTE, IDIOMA INCORECTO.")

while idioma_usuari != "Q":

    dia = input("\n1 - 7 (Q = quit, sortir, salir): ")

    if dia.lower() == "q":
        exit()
    elif dia == "1":
        if idioma_usuari.lower() == "english":
            print("MONDAY")
        elif idioma_usuari.lower() == "catala":
            print("DILLUNS")
        else:
            print("LUNES")
    elif dia == "2":
        if idioma_usuari.lower() == "english":
            print("TUESDAY")
        elif idioma_usuari.lower() == "catala":
            print("DIMARTS")
        else:
            print("MARTES")
    elif dia == "3":
        if idioma_usuari.lower() == "english":
            print("WEDNESDAY")
        elif idioma_usuari.lower() == "catala":
            print("DIMECRES")
        else:
            print("MIERCOLES")
    elif dia == "4":
        if idioma_usuari.lower() == "english":
            print("THURSDAY")
        elif idioma_usuari.lower() == "catala":
            print("DIJOUS")
        else:
            print("JUEVES")
    elif dia == "5":
        if idioma_usuari.lower() == "english":
            print("FRIDAY")
        elif idioma_usuari.lower() == "catala":
            print("DIVENDRES")
        else:
            print("VIERNES")
    elif dia == "6":
        if idioma_usuari.lower() == "english":
            print("SATURDAY")
        elif idioma_usuari.lower() == "catala":
            print("DISSABTE")
        else:
            print("SABADO")
    elif dia == "7":
        if idioma_usuari.lower() == "english":
            print("SUNDAY")
        elif idioma_usuari.lower() == "catala":
            print("DIUMENGE")
        else:
            print("DOMINGO")
    else:
        if idioma_usuari.lower() == "english":
            print("That day does not exist!")
        elif idioma_usuari.lower() == "catala":
            print("No existeix!")
        else:
            print("Ese dia no existe!")
