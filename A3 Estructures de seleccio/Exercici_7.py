"""
UMAR MOHAMMAD RIAZ
20/10/2021
ASIXc 1B

Crea un programa que donats dos nombres entrats per teclat, la base i l'exponent, calculi la potència i la mostri a
la pantalla. El programa ha de controlar el següent:

* L'exponent és positiu, només cal calcular la potència amb l'operador corresponent de Python
* L'exponent és 0, el resultat és 1, sigui quina sigui la base.
* L'exponent és negatiu, el resultat s'obté calculant primer la potència P amb l'exponent passat a nombre positiu
i calculant després 1 / P.

Nota: En Àlgebra i en Combinatòria, generalment el valor de 0 0 és considerat com 1, mentre que en anàlisi matemàtica
l'expressió és de vegades deixada indefinida. En el nostre cas considerarem que val 1.

"""

base = float(input("ENTRA LA BASE: "))
exponent = float(input("ENTRA EL EXPONENT: "))

if exponent < 0:
    print("EXPONENT NEGATIU!")
else:
    potencia = base ** exponent
    print(f"RESULTAT: {potencia}")

print("\nPROGRAMA FINALITZAT")
