"""
UMAR MOHAMMAD RIAZ
20/10/2021
ASIXc 1B

Escriu un programa que demani dos nombres per teclat i mostri la seva divisió. El programa ha de comprovar si el
segon nombre és igual a zero. Si ho és en lloc de realitzar i mostrar el resultat de la divisió ha de mostrar un
missatge d'error per pantalla indicant que la divisió no es pot realitzar, ja que el segon nombre és zero.
"""


numeros = input("Entra dos numeros separats per un espai: ")

numeroslista = numeros.split(" ")

a = float(numeroslista.pop(0))
b = float(numeroslista.pop(0))

if b == 0:
    print("\nLa divisió no es pot realitzar, ja que el segon nombre és zero.")
else:
    resultat = a / b
    print(f"{a} / {b} = {round(resultat, 2)}")

print("\nPROGRAMA FINALITZAT")
