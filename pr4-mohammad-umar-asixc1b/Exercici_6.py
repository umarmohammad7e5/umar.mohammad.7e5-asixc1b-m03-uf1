"""
UMAR MOHAMMAD RIAZ
16/11/2021
ASIXc 1B

Programa que calcula el factorial d'un número n. El factorial de n, n!= n * n-1*n-2*….*3*2*1
"""
factorial = int(input("Factorial de: "))

RESULTAT = 1
numero = 1
while numero <= factorial:
    RESULTAT = RESULTAT * numero
    numero = numero + 1

print(RESULTAT)
