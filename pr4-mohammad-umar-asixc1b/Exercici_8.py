"""
UMAR MOHAMMAD RIAZ
16/11/2021
ASIXc 1B

Programa que demani una cadena de caracteres, imprimeix únicament les vocals que hi hagi.
"""

frase = str(input("Escriu una frase: "))

vocals = ["a", "e", "i", "o", "u"]

for lletra in frase:
    for vocal in vocals:
        if lletra == vocal:
            print(vocal, end=" ")

print("\nfi!")
