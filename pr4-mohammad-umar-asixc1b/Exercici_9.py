"""
UMAR MOHAMMAD RIAZ
16/11/2021
ASIXc 1B

Programa que demana un número a l'usuari i mostra per pantalla els seus divisors.
"""

numero = int(input("Instrodueix un numero: "))

for x in range(1, numero):
    if numero % x == 0:
        print(str(x) + " es divisor de " + str(numero))

print("FI!")
