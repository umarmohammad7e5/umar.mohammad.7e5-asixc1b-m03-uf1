"""
UMAR MOHAMMAD RIAZ
16/11/2021
ASIXc 1B

Programa que imprimeix un tauler d’escacs per pantalla. Un taulell d’escacs comença amb la casella  Blanca
i és de mida 8x8 sempre ;-)
"""

MIDA = int(input("Introdueix la mida: "))

if MIDA % 2 == 0:
    x = MIDA // 2
    for i in range(MIDA // 2):
        print(f"███   " * x)
        print(f"   ███" * x)
else:
    x = MIDA // 2
    for i in range(MIDA // 2):
        print(f"███   " * x, end="███\n")
        print(f"   ███" * x, end="   \n")
    print(f"███   " * x, end="███\n")
