"""
UMAR MOHAMMAD RIAZ
16/11/2021
ASIXc 1B

Programa que demana a l'usuari la introducció de 10 nombres sencers i ha de mostrar, al final i per pantalla,
quants són positius, quants negatius i quants zero.
"""

POSITIUS = 0
NEGATIUS = 0
ZEROS = 0

numeros = input("Introdueix 10 numeros separats per un espai: ")
numerosllista = numeros.split(" ")


while len(numerosllista) != 10:
    print("NO HI HA 10 NUMEROS! Hi ha " + str(len(numerosllista)))
    numeros = input("\nIntrodueix 10 numeros separats per un espai: ")
    numerosllista = numeros.split(" ")

for numero in numerosllista:
    if float(numero) == 0:
        ZEROS += 1
    elif float(numero) > 0:
        POSITIUS += 1
    else:
        NEGATIUS += 1

print("\nZEROS: " + str(ZEROS))
print("POSITIUS: " + str(POSITIUS))
print("NEGATIUS: " + str(NEGATIUS))







