"""
UMAR MOHAMMAD RIAZ
16/11/2021
ASIXc 1B

Programa que mostra per pantalla la suma de tots els nombres senars i de tots els nombres parells inferiors
a un número límit, que l’usuari introdueix per teclat. ( Ex: sí el límit és 31 sumaParells 240 i sumaSenars 225)
"""

numerolimit = int(input("Introduiex un numero: "))
SUMAPARELLS = 0
SUMASENARS = 0

for numero in range(numerolimit):
    if numero % 2 == 0:
        SUMAPARELLS = SUMAPARELLS + numero
    else:
        SUMASENARS = SUMASENARS + numero

print(f"\nSuma dels parells: {SUMAPARELLS}")
print(f"Suma dels senars: {SUMASENARS}")
