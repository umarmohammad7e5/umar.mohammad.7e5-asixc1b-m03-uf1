"""
UMAR MOHAMMAD RIAZ
16/11/2021
ASIXc 1B

Programa que mostra per pantalla tots els nombres parells menors que 20.
"""

for numero in range(20):
    if numero % 2 == 0:
        print(numero)

print("\nFI!")
