"""


Crea una aplicació que permeti endevinar un número. L'aplicació genera un nombre “aleatori” de l'1 al 100. A
continuació, l’aplicació va demanant números i va responent si el nombre a endevinar és més gran o més petit que
l'introduït, a més dels intents que et queden (tens 10 intents per encertar-lo). El programa acaba quan s'encerta
el número (a més et diu quants intents has necessitat per encertar-lo), si s'arriba al límit d'intents, l’aplicació
et mostra el número que havia generat.
"""
import random

numero = random.randint(1, 100)

MAX_INTENTS = 10
intents = MAX_INTENTS

while intents != 0:
    intents -= 1
    usuari = int(input("\nDigues el número de 1 al 100: "))
    if usuari != numero:
        print(f"\nNO! Et quedan {intents} intents!")
        if int(usuari) < int(numero):
            print("El número es més gran!")
        else:
            print("El número es més Petit!")
    else:
        print(f"\nSÍ! Has adivinat en {MAX_INTENTS - intents} intents")
        exit()

print(f"\nNO! Intents agotats, el número era {numero}")
