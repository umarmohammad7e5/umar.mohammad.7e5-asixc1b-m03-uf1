"""

UMAR MOHAMMAD RIAZ
30/11/2021
ASIXc 1B

Escriu un programa que mostri el següent menú d’opcions:
Literatura
Cinema
Música
Videojocs
Sortir
Si l’usuari tria una opció de l’1 al 4, el programa ha de mostrar  uns quants suggeriments de títols relacionats amb
el tema escollit. Si l’usuari tria una opció no contemplada, el programa ha de mostrar un missatge d’error. En tot cas,
el programa tornarà a mostrar el menú d’opcions, tret que l’usuari triï l’opció 5: en aquest cas, el programa mostrarà
un missatge de comiat i acabarà.

"""


menu = print(" 1. Literatura"
             "\n 2. Cinema"
             "\n 3. Musica"
             "\n 4. Videojocs"
             "\n 5. Sortir")

opcio = input("\nEscull una opció [1 - 5]: ")

while opcio != "5":
    if opcio == "1":
        print("\nAquestes son les meves recomenacions de LITERATURA:"
              "\n   La educación sentimental de Gustave Flaubert	(1869)"
              "\n   Romancero gitano de Federico García Lorca	(1928)"
              "\n   Cien años de soledad de Gabriel García Márquez	(1967)"
              "\n   El amor en los tiempos del cólera de Gabriel García Márquez	(1985)")
    elif opcio == "2":
                print("\nAquestes son les meves recomenacions de CINEMA:"
              "\n   El padrino» (1972)"
              "\n   Pulp Fiction (1994)"
              "\n   E.T., el extraterrestre (1982)")
    elif opcio == "3":
        print("\nAquestes son les meves recomenacions de CINEMA:"
              "\n   El padrino» (1972)"
              "\n   Pulp Fiction (1994)"
              "\n   E.T., el extraterrestre (1982)")
    elif opcio == "4":
        print("\nAquestes son les meves recomenacions de CINEMA:"
              "\n   El padrino» (1972)"
              "\n   Pulp Fiction (1994)"
              "\n   E.T., el extraterrestre (1982)")
    else:
        opcio = input("\nEscull una opció [1 - 5]: ")

    opcio = input("\nEscull una opció [1 - 5]: ")
