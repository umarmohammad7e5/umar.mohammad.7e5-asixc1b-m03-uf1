"""
  Umar Mohammmad Riaz
  01/12/2021
  ASIXc1A M03 UF1

  PIRAMIDE - ARBRE
"""
from time import sleep

nivells = int(input("Quants nivells vols?: "))

base = nivells * 2 - 1
print((" 🌟").center(nivells * 2))
for num in range(nivells - 1):
    #print(" " * (nivells - num) + "#" * (2 * num + 1))
    print(" " * (nivells - num) + "#" + "-" * ((2 * num + 1) - 1) + "#")
    sleep(0.2)
print(" " * ((nivells - num) - 1) + "#" * base + "#")
#Calcular i dibuixar el tronc


meitat_base = base // 2
altura_tronc = nivells * 0.3

for x in range(round(altura_tronc)):
    print(" " * round(meitat_base * 0.7), end=" ")
    if base % 2 == 0:
        print(" ", end="")
        print("|" * int(nivells // 2))
    else:
        print("|" * int(nivells // 2 + 1))

print()
print(("|🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟|").center(nivells * 2))
print((" |🌟🌟 FELIZ NAVIDAD🌟🌟|").center(nivells * 2))
print(("|🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟🌟|").center(nivells * 2))
