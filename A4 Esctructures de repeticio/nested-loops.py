#Using FOR
"""
for i in range(0, 11):
   for j in range(0, 11):
       print("%d x %d = %d"%(i, j, i*j))
   print("+----------+")

#Using WHILE
"""

contI = 1
contJ = 1
while contI <= 10:
    contJ = 0
    while contJ <= 10:
       print("%d x %d = %d" % (contI, contJ, contI*contJ))
       contI += 1
    print("---------------")
    contJ += 1




#Does it works ?

#   contI+=1
