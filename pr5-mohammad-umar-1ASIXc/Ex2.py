ººº"""
Umar Mohammad Riaz
1r ASIXc
18/01/2022

2. Programa que generi una llista de 100 nombres aleatoris entre 1 i 50. Obtenir la mitja dels nombres que es
troben a les posicions parelles i la mitja del nombre de les  posicions senars.

Per aconseguir nombres aleatoris en Python podem utilitzar la funció random.randint(limitInferior,limitSuperior)
# Program to generate a random number between 0 and 9
# importing the random module
import random
print(random.randint(0,9))"""
import random

llistanum = []
MIDA_LLISTA = 100

for num in range(MIDA_LLISTA):
    numaleatori = random.randint(1, 50)
    llistanum.append(numaleatori)

parells = []
senars = []

contador = 0
for num in llistanum:
    if contador % 2 == 0:
        parells.append(num)
    else:
        senars.append(num)
    contador += 1

mitjana_parells = round(sum(parells) / len(parells))
mitjana_senars = round(sum(senars) / len(senars))

print(f"Mitjana de posicions parells: {mitjana_parells}"
      f"\nMitjana de posicions senars: {mitjana_senars}")

print("\nPROGRAMA FINALITZAT!")
