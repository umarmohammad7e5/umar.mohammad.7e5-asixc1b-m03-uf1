"""
Umar Mohammad Riaz
1r ASIXc
18/01/2022

1. Programa de càlcul de temperatures emmagatzemades dins d’una llista de nombres sencers (sense decimals).
Proveu el resultat usant les definicions següents d’array, una per a cada execució diferent:
//Primer proveu si funciona usant aquesta llista
gener = [1, 12, 4, -5, 7, 3, 2,
        9, -6, 7, 8, 2, 14, -7,
        5, 13, 12, 4, 6, -7, 1,
        12, 4, -2, 3, 5, 2, 7,
        -2, 8,-15]

Cal definir els 3 primers mesos de l’any, i calcular:
Per a cada mes: temperatura màxima, mínima i mitjana
Per al 1r trimestre de l’any: temperatura màxima, mínima i mitjana

"""

gener = [1, 12, 4, -59, 7, 3, 2,
         9, -6, 7, 8, 2, 14, -7,
         5, 13, 12, 4, 6, -7, 1,
         12, 4, -2, 3, 5, 2, 7,
         -2, 8, -15]

febrer = [1, 12, 4, -5, 7, 3, 2,
          9, -6, 7, 8, 2, 14, -7,
          5, 13, 12, 4, 6, -7, 1,
          12, 4, -2, 3, 5, 29, 7]

marc = [1, 12, 4, -5, 7, 3, 2,
        9, -6, 7, 8, 2, 14, -7,
        5, 63, 12, 4, 6, -7, 1,
        12, 4, -2, 3, 5, 2, 7,
        -2, 8, -3]

trimestre = gener + febrer + marc


def maxim(mes):
    maxim = mes[0]
    for temp in mes:
        if temp > maxim:
            maxim = temp
    return maxim


def minim(mes):
    minim = mes[0]
    for temp in mes:
        if temp < minim:
            minim = temp
    return minim


def mitja(mes):
    suma = 0
    for num in mes:
        suma += num
    mitjana = round(suma / len(mes), 1)
    return mitjana


print(f"GENER:\n Tº MAX: {maxim(gener)} \n Tª MIN: {minim(gener)} \n Tª MITJANA: {mitja(gener)}\n")
print(f"FEBRER:\n Tº MAX: {maxim(febrer)} \n Tª MIN: {minim(febrer)} \n Tª MITJANA: {mitja(febrer)}\n")
print(f"MARÇ\n Tº MAX: {maxim(marc)} \n Tª MIN: {minim(marc)} \n Tª MITJANA: {mitja(marc)}\n")
print(f"TRIMESTRE:\n Tº MAX: {maxim(trimestre)} \n Tª MIN: {minim(trimestre)} \n Tª MITJANA: {mitja(trimestre)}\n")

print("\nPROGRAMA FINALITZAT!")
