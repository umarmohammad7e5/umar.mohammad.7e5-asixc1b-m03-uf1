"""2.Feu un programa que vagi llegint frases pel teclat i, en acabar cada entrada d'una frase, mostri la frase
encriptada. Per encriptar la frase, ha de canviar les vocals per numeros del 1 al 5. No fer
diferències entre majúsucles i minúscules ni lletras amb accents, totes s'han de tractar com si
fossin minúscules i sense accent."""


uno = ["a", "á", "à"]
dos = ["e", "é", "è"]
tres = ["i", "í", "ì", "ï"]
cuatro = ["o", "ó", "ò"]
cinco = ["u", "ú", "ù", "ü"]

frase = input("Frase a encriptar: ")

for lletra in frase:
    if lletra in uno:
        print("1", end="")
    elif lletra in dos:
        print("2", end="")
    elif lletra in tres:
        print("3", end="")
    elif lletra in cuatro:
        print("4", end="")
    elif lletra in cinco:
        print("5", end="")
    else:
        print(lletra, end="")


print("\nPROGRAMA FINALIZAT")
