"""1.Fer un programa per demanar el número de files i columnes, d'una matriu. Comprovar que la matriu sigui quadra, és a dir
que tingui les mateixes fileres que columnes. I que sigui un número senar de fileres i columnes. Omplir la matriu
amb 0's tret de la primera i última columna i la filera central, que s'han d'omplir amb 1's. Mostrar la matriu
resultant per pantalla.
El resultat ha de dibuixar una H amb els 1's dins de la matriu de 0's"""


columna = int(input("\nIntrodueix el número de columnes: "))
fila = int(input("Introdueix el número de files: "))

while columna != fila or columna % 2 == 0 and fila % 2 == 0:
    print("\nEl número de files i columnes ha ser IGUAL I SENAR!")
    columna = int(input("\nIntrodueix el número de columnes: "))
    fila = int(input("Introdueix el número de files: "))

print()
for y in range(columna):
    for x in range(fila):
        if y == (columna // 2):
            print(" 1 ", end="")
        elif x == 0 or x == fila - 1:
            print(" 1 ", end="")
        else:
            print(" 0 ", end="")

    print("")



"""filasYcolumnas = int(input("Número de filas y columnas: "))
if filasYcolumnas % 2 == 0:
    print("Incorrecto, el número debe ser senar")
else:
    for filas in range(filasYcolumnas):
        for columnas in range(filasYcolumnas):
            if columnas == 0 or columnas == filasYcolumnas - 1:
                print("1", end="")
            elif filas == filasYcolumnas//2:
                print("1", end="")
            else:
                print("0", end="")
        print("")"""
