"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B

Implementa un programa que demani per teclat dos punts del pla i tot seguit calculi i mostri la distància entre ells.

Notes:

Un punt del pla es representa amb un parell de números. Per exemple, el punt A es representarà pels números x1 i y1, i
el punt B pels números x2 i y2
La fórmula de la distància entre dos punts A i B és:

distància(A,B) = √( (x2 - x1)2 + (y2 - y1)2)

"""
import math

punt_x = input("Punt x [a, b]: ")
punt_y = input("Punt y [a, b]: ")

punt_x_lista = punt_x.split(",")
punt_y_lista = punt_y.split(",")

ax = float(punt_x_lista.pop(0))
bx = float(punt_x_lista.pop(0))
ay = float(punt_y_lista.pop(0))
by = float(punt_y_lista.pop(0))


formula_part1 = (ax - ay)
formula_part2 = (bx - by)

formula_e2 = (formula_part1 ** 2) + (formula_part2 ** 2)
resultat = math.sqrt(formula_e2)

print(f"Resultat {round(resultat, 2)}")
