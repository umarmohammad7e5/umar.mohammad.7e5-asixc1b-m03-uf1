
"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B

Escriure un programa que pregunti a l'usuari pel seu nom de pila, i tot seguit escriviu el missatge "Hola, XXX!"
on XXX és el nom de pila introduït per l'usuari. Per exemple, si l'usuari entra "Javi", el missatge serà "Hola, Javi!"
"""

name = input("Com et dius?: ")

print("Hola " + name)
