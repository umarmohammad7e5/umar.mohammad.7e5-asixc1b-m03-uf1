"""
UMAR MOHAMMAD RIAZ
13/10/2021
ASIXc 1B

Dos vehicles A i B viatgen a velocitats vA i vB respectivament, amb vB> vA. El vehicle B va a una
distància D darrere del vehicle A.

Es demana implementar un programa que demani les velocitats dels vehicles, expressades en km / h, i
la distància entre ells (en km) Amb aquestes dades el programa ha de calcular i mostrar el temps en
minuts en què el vehicle B arribarà a l’alçada del vehicle A.

"""
velocitat_cotxe_b = float(input("Velocitat del coche b em KM/H: "))

distancia_km = int(input("Introduiex la distancia entre coche a i b en KM: "))

velocitat = velocitat_cotxe_b / 3.6
distancia = distancia_km * 1000

temps_sec = distancia / velocitat
temps = temps_sec / 60
print()
print(f"Tardara {temps} mins")

