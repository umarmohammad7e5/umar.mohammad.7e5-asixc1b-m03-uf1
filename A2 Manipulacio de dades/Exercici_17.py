"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B

Un ciclista triga T segons a recórrer el trajecte de la ciutat “A” a la ciutat “B”. El ciclista va partir de
la ciutat “A” a les HH hores, MM minuts i SS segons.

Escriu un programa que calculi l'hora d'arribada a la ciutat B. Els segons en què el ciclista tarda a realitzar
el trajecte i l'hora, minuts i segons de partida es demanaran per teclat.
"""
import datetime


trajecte = int(input("Quant tarda en arribar de A a B en segons: "))
hora_sortida = input("Introduiex la hora de sortida com HH MM SS: ")

hora_sortida_llista = hora_sortida.split(" ")

sortida_hora = int(hora_sortida_llista.pop(0))
sortida_min = int(hora_sortida_llista.pop(0))
sortida_sec = int(hora_sortida_llista.pop(0))


temps_trajecte = trajecte / 60

mins = temps_trajecte % 60
hora = (temps_trajecte // 60)
secs = trajecte % 60



sortida = datetime.timedelta(hours=sortida_hora, minutes=sortida_min, seconds=sortida_sec)
trajecte_temps = datetime.timedelta(hours=hora, minutes=mins, seconds=secs)

arribada = sortida + trajecte_temps



print()
print(f"Arribará a les {arribada}")

