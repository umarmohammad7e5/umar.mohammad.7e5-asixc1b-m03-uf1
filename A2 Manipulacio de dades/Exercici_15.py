"""
UMAR MOHAMMAD RIAZ
13/10/2021
ASIXc 1B

Escriu el programa que demani a l'usuari el valor de dues variables A i B, intercanviï els seus valors i
finalment els mostri.

"""

A = input("Contingut A: ")
B = input("Contingut B: ")
print()
print("A = " + A)
print("B = " + B)

C = A
A = B
B = C
print()
print("Intercanvi:")
print("A = " + A)
print("B = " + B)
