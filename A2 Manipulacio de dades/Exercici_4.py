"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B

Escriu el programa que calcula i mostra en pantalla la suma, resta, divisió i multiplicació de dos nombres
entrats per l'usuari.

"""


num_a = int(input("Introdueix el número a: "))
num_b = int(input("Introdueix el número b: "))

suma = str(num_a + num_b)
resta = str(num_a - num_b)
divisio = str(num_a / num_b)
multiplicacio = str(num_a * num_b)


print()
print(str(num_a) + " + " + str(num_b) + " = " + suma)
print(str(num_a) + " - " + str(num_b) + " = " + resta)
print(str(num_a) + " : " + str(num_b) + " = " + divisio)
print(str(num_a) + " x " + str(num_b) + " = " + multiplicacio)
