"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B


En un institut la nota de l'assignatura de Programació es calcula de la següent manera:

     • 55% de la mitjana de tres qualificacions parcials.
     • 30% de la nota d’una prova final.
     • 15% de la nota d’un treball final.

Implementa un programa que calculi la nota final a partir dels valors entrats per teclat corresponents a les
qualificacions parcials i notes de la prova i treball finals.

"""

parcial_a = float(input("Nota Examen A "))
parcial_b = float(input("Examen B: "))
parcial_c = float(input("Examen c: "))
print()
mitjana = (parcial_a + parcial_b + parcial_c) / 3
prova_final = float(input("Nota de prova final: "))

print()
treball = int(input("Nota del treball: "))

nota_final = (mitjana * 0.55) + (prova_final * 0.3) + (treball * 0.15)

print()
print(f"La teva nota final és {round(nota_final, 2)}")
