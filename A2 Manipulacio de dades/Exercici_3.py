"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B

Donats els catets a i b d'un triangle rectangle, escriure el codi Python que calcula la seva hipotenusa.
Recordar que la hipotenusa es calcula de la següent manera (Teorema de Pitàgores):

c2 = a2 + b2 , on c és la hipotenusa. Per tant  c = √(a2 + b2)"
"""
import math

catet_a = input("Introduiex el catet a: ")
catet_b = input("Introduiex el catet b: ")



catet_a_2 = int(catet_a)**2
catet_b_2 = int(catet_b)**2
catet_ab = int(catet_a_2 + catet_b_2)


hipotenusa = float(math.sqrt(catet_ab))

print()
print(f"La HIPOTENUSA es: {round(hipotenusa, 2)}")


