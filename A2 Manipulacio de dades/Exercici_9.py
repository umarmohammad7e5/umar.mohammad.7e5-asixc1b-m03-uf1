"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B

Escriu un programa que donat l'import d'una compra, apliqui a aquest import un descompte del 15%
 i el mostri a pantalla.
 """

compra = float(input("Introdueix l'import: "))

descompte = compra * 0.15

preu_final = compra - descompte

print()
print(f"Descompte: {descompte}€")
print(f"Preu final amb descompte {preu_final}€")
